# Projet de mini site

Ce projet permet de construire et déposer sur le registre une image docker simple.

## Amorçage du projet 

``` shell

mkdir site1
cd site1
git init --initial-branch= main
```

# Ajout d'un README.md

Crée par la commande : ```micro README.md``` 

## Ajout du suivi de ce fichier 

``` shell
git add README.md
git commit -m "Initialisation du projet"
```

Alternativement, un ```git add .``` fera suivre tout les fichiers par git, hormis les fichiers exemptés grâce au fichier **.gitignore**,
à placer dans le même dossier.

Exemple de fichier .gitignore

```
.git
.gitignore
private

```
Dans le fichier .gitignore, un fichier ou un dossier peut être exclu du suivi de git en cas de commit

## Pousser un commit sur un dépôt distant

### Créer le dépôt 
Sur Gitlab ou Github

### Disposer d'au moins un commit
Il a été fait précédemment 

### Raccorder notre branche locale au dépôt distant

```shell
git remote add nom-remote la-reference-https-ou-ssh
```

### Git push

```
git push --set-upstream [le-nom-du-remote] [la-branche]
```

> sur le premier `git push`, il faut *raccorder l branche à l'origine* `--set-upstream`, mais
> sur les suivant, pas besoin. Un `git push` suffira.
